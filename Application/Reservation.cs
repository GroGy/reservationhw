using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Configuration;
using System.Text;

namespace DURezervace.Application
{
    public class Reservation
    {
        public int m_id;
        public DateTime m_date;
        public int m_carId;
        public int m_userID;

        public static Reservation CreateFromSerializedString(ref string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new Exception("Failed to parse reservation file");
            }

            string[] values = data.Split('|');

            if (values.Length < 4)
            {
                throw new Exception("Failed to parse reservation file");
            }

            // ID | DATE | CAR | USER_ID
            var result = new Reservation
            {
                m_id = Convert.ToInt32(values[0]),
                m_date = Convert.ToDateTime(values[1]),
                m_carId = Convert.ToInt32(values[2]),
                m_userID = Convert.ToInt32(values[3]),
            };

            return result;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(m_id);
            sb.Append('|');
            sb.Append(m_date);
            sb.Append('|');
            sb.Append(m_carId);
            sb.Append('|');
            sb.Append(m_userID);
            
            return sb.ToString();
        }
    }

    public class Reservationeer
    {
        private int m_pNextReservationID;
        private int m_pNextCarID;

        public Reservationeer()
        {
            if (!File.Exists(Application.path + "/resID.txt"))
            {
                var sw = new StreamWriter(Application.path + "/resID.txt");
                m_pNextReservationID = 0;
                sw.WriteLine(0);
                sw.Flush();
                sw.Close();
            }
            else
            {
                using (var sr = new StreamReader(Application.path + "/resID.txt"))
                {
                    try
                    {
                        m_pNextReservationID = Convert.ToInt32(sr.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to fetch next ID in db");
                        //TODO: Add part of code, that will find next highest id in db
                        throw;
                    }
                }
            }

            if (!File.Exists(Application.path + "/carID.txt"))
            {
                var sw = new StreamWriter(Application.path + "/carID.txt");
                m_pNextCarID = 0;
                sw.WriteLine(0);
                sw.Flush();
                sw.Close();
            }
            else
            {
                using (var sr = new StreamReader(Application.path + "/carID.txt"))
                {
                    try
                    {
                        m_pNextCarID = Convert.ToInt32(sr.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to fetch next ID in db");
                        //TODO: Add part of code, that will find next highest id in db
                        throw;
                    }
                }
            }
        }

        public List<Reservation> GetUserReservations(ref string name)
        {
            if (!Application.GetInstance().m_loginManager.UserExists(ref name))
            {
                throw new Exception("ERROR: User with entered name does not exist");
            }

            var result = new List<Reservation>(128);

            var userData = Application.GetInstance().m_loginManager.GetUserData(ref name);

            foreach (var f in Directory.GetFiles(Application.path + "/reservations"))
            {
                string fileName = f.Replace(Application.path + "/reservations/", "");
                string finalName = Path.GetFileNameWithoutExtension(fileName);

                var parts = finalName.Split('_');

                if (parts.Length < 3)
                {
                    continue;
                }

                int reservationID = Convert.ToInt32(parts[0]);
                int userID = Convert.ToInt32(parts[1]);
                int carID = Convert.ToInt32(parts[2]);

                if (userData.m_id == userID)
                {
                    using (var sr = new StreamReader(fileName))
                    {
                        string reservationData = sr.ReadLine();
                        result.Add(Reservation.CreateFromSerializedString(ref reservationData));
                    }
                }
            }

            return result;
        }

        public List<Reservation> GetAllReservations()
        {
            var result = new List<Reservation>(128);

            foreach (var f in Directory.GetFiles(Application.path + "/reservations"))
            {
                string fileName = f.Replace(Application.path + "/reservations/", "");
                string finalName = Path.GetFileNameWithoutExtension(fileName);

                var parts = finalName.Split('_');

                if (parts.Length < 3)
                {
                    continue;
                }

                using (var sr = new StreamReader(fileName))
                {
                    string reservationData = sr.ReadLine();
                    result.Add(Reservation.CreateFromSerializedString(ref reservationData));
                }
            }

            return result;
        }

        public void ReservationRemove(ref int id)
        {
            foreach (var f in Directory.GetFiles(Application.path + "/reservations"))
            {
                string fileName = f.Replace(Application.path + "/reservations/", "");
                string finalName = Path.GetFileNameWithoutExtension(fileName);

                var parts = finalName.Split('_');

                if (parts.Length < 3)
                {
                    continue;
                }

                int reservationID = Convert.ToInt32(parts[0]);
                int userID = Convert.ToInt32(parts[1]);
                int carID = Convert.ToInt32(parts[2]);

                if (reservationID == id)
                {
                    File.Delete(fileName);
                    break;
                }
            }
        }

        public void CarSave(ref Car car)
        {
            int id = car.m_id;

            var sw = new StreamWriter(Application.path + "/cars/" + id + ".txt");

            sw.WriteLine(car.ToString());

            sw.Flush();
            sw.Close();
        }

        public void CarRemove(ref int id)
        {
            File.Delete(Application.path + "/cars/" + id + ".txt");
            //TODO: Delete reservation if exist
        }

        public int GetNextReservationId()
        {
            int result = m_pNextReservationID++;

            var sw = new StreamWriter(Application.path + "/resID.txt");
            sw.WriteLineAsync(Convert.ToString(m_pNextReservationID)).ContinueWith(r =>
            {
                sw.Flush();
                sw.Close();
            });

            return result;
        }

        public int GetNextCarId()
        {
            int result = m_pNextCarID++;

            var sw = new StreamWriter(Application.path + "/carID.txt");
            sw.WriteLineAsync(Convert.ToString(m_pNextCarID)).ContinueWith(r =>
            {
                sw.Flush();
                sw.Close();
            });

            return result;
        }

        public bool CarExists(ref int id)
        {
            return File.Exists(Application.path + "/cars/" + id + ".txt");
        }

        public List<Car> CarGetList()
        {
            var result = new List<Car>(128);

            foreach (var a in Directory.GetFiles(Application.path + "/cars"))
            {
                using (var sr = new StreamReader(a))
                {
                    var data = sr.ReadLine();
                    try
                    {
                        result.Add(Car.CreateFromSerializedString(ref data));
                    }
                    catch (Exception e)
                    {
                        //FIXME: Error handling 
                    }
                }
            }

            return result;
        }

        public bool CarHasReservation(ref int id)
        {
            foreach (var f in Directory.GetFiles(Application.path + "/reservations"))
            {
                string fileName = f.Replace(Application.path + "/reservations/", "");
                string finalName = Path.GetFileNameWithoutExtension(fileName);

                var parts = finalName.Split('_');

                if (parts.Length < 3)
                {
                    continue;
                }

                int reservationID = Convert.ToInt32(parts[0]);
                int userID = Convert.ToInt32(parts[1]);
                int carID = Convert.ToInt32(parts[2]);

                if (carID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public void ReservationCreate(ref Reservation reservation)
        {
            if (CarHasReservation(ref reservation.m_carId))
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(reservation.m_id);
            sb.Append('_');
            sb.Append(reservation.m_userID);
            sb.Append('_');
            sb.Append(reservation.m_carId);
            sb.Append(".txt");
            
            var sw = new StreamWriter(Application.path + "/reservations/" + sb.ToString());
            sw.WriteLine(reservation.ToString());
            sw.Flush();
            sw.Close();
        }

        public List<Reservation> GetCarReservations(ref int carId)
        {

            var result = new List<Reservation>(128);

            foreach (var f in Directory.GetFiles(Application.path + "/reservations"))
            {
                string fileName = f.Replace(Application.path + "/reservations/", "");
                string finalName = Path.GetFileNameWithoutExtension(fileName);

                var parts = finalName.Split('_');

                if (parts.Length < 3)
                {
                    continue;
                }

                int reservationID = Convert.ToInt32(parts[0]);
                int userID = Convert.ToInt32(parts[1]);
                int carID = Convert.ToInt32(parts[2]);

                if (carID == carId)
                {
                    using (var sr = new StreamReader(fileName))
                    {
                        string reservationData = sr.ReadLine();
                        result.Add(Reservation.CreateFromSerializedString(ref reservationData));
                    }
                }
            }

            return result;
        }
    }
}