using System;

namespace DURezervace.Application
{
    public class Service
    {
        public int m_id;
        public decimal m_price;
        public DateTime m_date;
        public static Service CreateFromSerializedString(ref string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new Exception("Failed to parse service");
            }

            string[] values = data.Split('-');

            if (values.Length < 3)
            {
                throw new Exception("Failed to parse service");
            }

            DateTime time = Convert.ToDateTime(values[2]);
            decimal price = Convert.ToDecimal(values[1]);
            int id = Convert.ToInt32(values[0]) ;
            
            // ID - PRICE - DATE
            var result = new Service
            {
                m_id =id,
                m_price = price,
                m_date = time,
            };

            return result;
        }
    }
}