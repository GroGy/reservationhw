using System;

namespace DURezervace.Application
{
    public class Application
    {
        private static Application m_pInstance;

        public static string path = "database";
        public static ref Application GetInstance()
        {
            if (m_pInstance == null)
            {
                m_pInstance = new Application();
            }

            return ref m_pInstance;
        }

        private bool m_shouldExit = false;
        public Commands m_commandManager = new Commands();
        public Login m_loginManager = new Login();
        public Reservationeer m_reservationManager = new Reservationeer();
        
        private void _ReadData()
        {
            
        }

        private void _Init()
        {
            
        }

        public void Start()
        {
            Console.WriteLine("Starting reservation app...");
            _Init();
            _BootSequence();
        }

        private void _BootSequence()
        {
            Console.Clear();
            while (!m_shouldExit)
            {
                _AwaitCommand();
            }
        }

        private void _AwaitCommand()
        {
            switch (m_loginManager.GetUserMode())
            {
                case UserMode.PUBLIC:
                {
                    Console.Write("> ");
                    break;
                }
                case UserMode.USER:
                {
                    Console.Write("$ ");
                    break;
                }
                case UserMode.ADMIN:
                {
                    Console.Write("# ");
                    break;
                }
            }
            string fetchedCommand = Console.ReadLine();
            bool commandFound = false;
            //Global
            if (fetchedCommand == null || !m_commandManager.m_GlobalCommands.ContainsKey(fetchedCommand))
            {
            } else {
                commandFound = true;
                m_commandManager.m_GlobalCommands[fetchedCommand].m_callback.DynamicInvoke();
            }
            
            //User
            if (m_loginManager.GetUserMode() == UserMode.ADMIN || m_loginManager.GetUserMode() == UserMode.USER)
            {
                if (fetchedCommand == null || !m_commandManager.m_UserCommands.ContainsKey(fetchedCommand))
                {
                    
                } else {
                    commandFound = true;
                    m_commandManager.m_UserCommands[fetchedCommand].m_callback.DynamicInvoke();
                }
            }

            //Admin
            if (m_loginManager.GetUserMode() == UserMode.ADMIN)
            {
                if (fetchedCommand == null || !m_commandManager.m_AdminCommands.ContainsKey(fetchedCommand))
                {
                } else {
                    commandFound = true;
                    m_commandManager.m_AdminCommands[fetchedCommand].m_callback.DynamicInvoke();
                }
            }

            if (!commandFound)
            {
                m_commandManager.m_GlobalCommands["help"].m_callback.DynamicInvoke();
            }
        }
    }
}