using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DURezervace.Application
{
    public enum UserMode : int
    {
        PUBLIC = 0,
        USER = 1,
        ADMIN = 2
    }

    
    public class Login
    {
        private User m_pCurrentUser;
        private int m_pNextID;

        private string _CalculatePasswordHash(ref string pwd)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(pwd));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }

        public UserMode GetUserMode()
        {
            return m_pCurrentUser.m_permission;
        }

        public Login()
        {
            if (!Directory.Exists(Application.path))
            {
                Directory.CreateDirectory(Application.path);
            }
            
            if (!File.Exists(Application.path + "/id.txt"))
            {
                var sw = new StreamWriter(Application.path + "/id.txt");
                m_pNextID = 0;
                sw.WriteLine(0);
                sw.Flush();
                sw.Close();
            }
            else
            {
                using (var sr = new StreamReader(Application.path + "/id.txt")) {
                    try
                    {
                        m_pNextID = Convert.ToInt32(sr.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to fetch next ID in db");
                        //TODO: Add part of code, that will find next highest id in db
                        throw;
                    }
                }
            }

            if (!Directory.Exists(Application.path + "/reservations"))
            {
                Directory.CreateDirectory(Application.path + "/reservations");
            }
            
            if (!Directory.Exists(Application.path + "/cars"))
            {
                Directory.CreateDirectory(Application.path + "/cars");
            }

            if (!Directory.Exists(Application.path + "/users"))
            {
                Directory.CreateDirectory(Application.path + "/users");
            }
            
            m_pCurrentUser = new User();
        }

        public bool UserExists(ref string name)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(Application.path);
                builder.Append("/users/");
                builder.Append(name);
                builder.Append(".txt");
                return File.Exists(builder.ToString());
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public User GetUserData(ref string name)
        {

            StringBuilder builder = new StringBuilder();
            builder.Append(Application.path);
            builder.Append("/users/");
            builder.Append(name);
            builder.Append(".txt");

            using (var sr = new StreamReader(builder.ToString())) {
                string data = sr.ReadLine();
                return User.CreateFromSerializedString(ref data);
            }
        }

        public int GetNextID()
        {
            int result = m_pNextID++;
            
            var sw = new StreamWriter(Application.path + "/id.txt");
            sw.WriteLineAsync(Convert.ToString(m_pNextID)).ContinueWith(r =>
            {
                sw.Flush();
                sw.Close();
            });
            
            return result;
        }

        public bool UserRegister(ref string name, ref string password)
        {
            if (UserExists(ref name)) {
                return false;
            }

            string pwdHash = _CalculatePasswordHash(ref password);

            int nextID = GetNextID();
            
            
            
            var data = new User()
            {
                m_id = nextID,
                m_pwHash = pwdHash,
                m_name = name,
                m_permission = UserMode.USER,
                m_lastLogin = DateTime.Now
            };

            UserSave(ref data, ref name);
            return true;
        }

        public bool UserLogin(ref string name, ref string password)
        {
            if (!UserExists(ref name)) return false;

            string pwdHash = _CalculatePasswordHash(ref password);
            var data = GetUserData(ref name);

            if (pwdHash == data.m_pwHash)
            {
                m_pCurrentUser = data;
                return true;
            }

            return false;
        }

        public void UserSave(ref User data, ref string name)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Application.path);
            builder.Append("/users/");
            builder.Append(name);
            builder.Append(".txt");
            
            StringBuilder sb = new StringBuilder();
            sb.Append(data.m_id);
            sb.Append('|');
            sb.Append(data.m_pwHash);
            sb.Append('|');
            sb.Append(data.m_name);
            sb.Append('|');
            sb.Append((int)data.m_permission);
            sb.Append('|');
            sb.Append(data.m_lastLogin);
            sb.Append('|');
            sb.Append(data.m_forcedToChangePWD ? "true" : "false");
            string dataString = sb.ToString();

            var sw = new StreamWriter(builder.ToString());
            sw.WriteLine(dataString);
            sw.Close();

        }

        public void UserLogout()
        {
            m_pCurrentUser = new User();
        }

        public void UserChangePassword(ref string newPwd1)
        {
            var newHash = _CalculatePasswordHash(ref newPwd1);
            m_pCurrentUser.m_pwHash = newHash;
            m_pCurrentUser.m_forcedToChangePWD = false;
            UserSave(ref m_pCurrentUser,ref m_pCurrentUser.m_name);
        }

        public void UserRemove(ref string login)
        {
            File.Delete(Application.path + "/users/" + login + ".txt");
        }

        public bool GetForcedPasswordChange()
        {
            return m_pCurrentUser.m_forcedToChangePWD;
        }

        public void UserForcePasswordExpire(ref string name)
        {
            var data = GetUserData(ref name);
            data.m_forcedToChangePWD = true;
            UserSave(ref data,ref data.m_name);
        }

        public string GetName()
        {
            return m_pCurrentUser.m_name;
        }

        public int GetUserID()
        {
            return m_pCurrentUser.m_id;
        }
    }
}