using System;

namespace DURezervace.Application
{
    public class User
    {
        public int m_id;
        public string m_name;
        public string m_pwHash;
        public UserMode m_permission;
        public DateTime m_lastLogin;
        public bool m_forcedToChangePWD;

        public User()
        {
            m_id = -1;
            m_name = null;
            m_pwHash = null;
            m_permission = UserMode.PUBLIC;
            m_lastLogin = DateTime.Now;
            m_forcedToChangePWD = false;
        }

        public static User CreateFromSerializedString(ref string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new Exception("Failed to parse user file");
            }

            string[] values = data.Split('|');

            if (values.Length < 6)
            {
                throw new Exception("Failed to parse user file");
            }
            // ID | PWD | NAME | PERMISSIONS | LAST LOGIN | HAS TO CHANGE PWD

            var result = new User
            {
                m_id = Convert.ToInt32(values[0]),
                m_pwHash = values[1],
                m_name = values[2],
                m_permission = (UserMode) Convert.ToInt32(values[3]),
                m_lastLogin = Convert.ToDateTime(values[4]),
                m_forcedToChangePWD = values[5] == "true"
            };

            return result;
        }
    }
}