using System;
using System.Collections.Generic;

namespace DURezervace.Application
{
    public delegate void Del();

    public class Commands
    {
        public Commands()
        {
            m_GlobalCommands = new Dictionary<string, Command>();
            m_UserCommands = new Dictionary<string, Command>();
            m_AdminCommands = new Dictionary<string, Command>();


            //----------------GLOBAL COMMANDS-----------------

            Del cmdHelp = CommandHelp;
            m_GlobalCommands["help"] = new Command(cmdHelp,"help : Shows help for all commands");
            Del cmdLogin = CommandLogin;
            m_GlobalCommands["login"] = new Command(cmdLogin, "login : Logs user to system");

            //-------------------------------------------------



            //------------------USER COMMANDS------------------

            Del cmdLogout = CommandUserLogout;
            m_UserCommands["logout"] = new Command(cmdLogout,"logout : Logout?");
            Del cmdChangePwd = CommandUserChangePassword;
            m_UserCommands["chpwd"] = new Command(cmdChangePwd,"chpwd : Change password");
            Del cmdGetReservations = CommandUserGetReservations;
            m_UserCommands["getReservations"] = new Command(cmdGetReservations,"getReservations : Gets all your reservations");
            Del cmdAddReservation = CommandUserAddReservation;
            m_UserCommands["addReservation"] = new Command(cmdAddReservation,"addReservation : Adds new reservation");
            Del cmdListCars = CommandUserListCars;
            m_UserCommands["listCars"] = new Command(cmdListCars,"listCars : Lists all cars in system");
            Del cmdUserRemoveReservation = CommandUserRemoveReservation;
            m_UserCommands["removeReservation"] = new Command(cmdUserRemoveReservation,"removeReservation : Removes your reservation");


            //-------------------------------------------------




            //------------------ADMIN COMMANDS-----------------

            Del cmdRegister = CommandAdminRegister;
            m_AdminCommands["register"] = new Command(cmdRegister,"register : Registers new user");
            //m_GlobalCommands["register"] = new Command(cmdRegister);

            Del cmdRemoveUser = CommandAdminRemoveUser;
            m_AdminCommands["removeUser"] = new Command(cmdRemoveUser,"removeUser : Removes user");

            Del cmdForcePwdChange = CommandAdminExpireUserPassword;
            m_AdminCommands["expirePassword"] = new Command(cmdForcePwdChange,"expirePassword : Forces password expiration on account");

            Del cmdAddCar = CommandAdminAddCar;
            m_AdminCommands["addCar"] = new Command(cmdAddCar,"addCar : Adds new car to system");

            Del cmdRemoveCar = CommandAdminRemoveCar;
            m_AdminCommands["removeCar"] = new Command(cmdRemoveCar,"removeCar : Removes car from system");

            Del cmdAdminRemoveReservation = CommandAdminRemoveReservation;
            m_AdminCommands["removeUserReservation"] = new Command(cmdAdminRemoveReservation,"removeUserReservation : Removes reservation of user");
            
            Del cmdGetAllCarReservations = CommandAdminListReservationsByCar;
            m_AdminCommands["getReservationsByCar"] = new Command(cmdGetAllCarReservations,"getReservationsByCar : Gets all reservations for car");
            
            Del cmdGetAllUserReservations = CommandAdminListReservationsByUser;
            m_AdminCommands["getReservationsByUser"] = new Command(cmdGetAllUserReservations,"getReservationsByUser : Gets all reservations for user");
            
            Del cmdGetAllReservations = CommandAdminListReservations;
            m_AdminCommands["getAllReservations"] = new Command(cmdGetAllReservations,"getAllReservations : Gets all reservations in system");



            //-------------------------------------------------

        }

        public Dictionary<string, Command> m_GlobalCommands;
        public Dictionary<string, Command> m_UserCommands;
        public Dictionary<string, Command> m_AdminCommands;

        public static void CommandHelp()
        {
            var um = Application.GetInstance().m_loginManager.GetUserMode();

            foreach (var cmd in Application.GetInstance().m_commandManager.m_GlobalCommands)
            {
                Console.WriteLine(cmd.Value.m_helpMessage);
            }

            if (um == UserMode.USER || um == UserMode.ADMIN)
            {
                foreach (var cmd in Application.GetInstance().m_commandManager.m_UserCommands)
                {
                    Console.WriteLine(cmd.Value.m_helpMessage);
                }
            }
            if (um == UserMode.ADMIN)
            {
                foreach (var cmd in Application.GetInstance().m_commandManager.m_AdminCommands)
                {
                    Console.WriteLine(cmd.Value.m_helpMessage);
                }
            }
        }

        public static void CommandLogin()
        {
            Console.Write("Enter login: ");
            string login = Console.ReadLine();
            if (login == null)
            {
                Console.WriteLine("Error: Failed to find account for entered login");
            }

            var app = Application.GetInstance();

            if (app.m_loginManager.UserExists(ref login))
            {
                Console.Write("Enter password: ");
                string pwd = Console.ReadLine();
                if (app.m_loginManager.UserLogin(ref login, ref pwd))
                {
                    Console.Clear();
                    Console.WriteLine("Successfully logged in.");
                    if (app.m_loginManager.GetForcedPasswordChange())
                    {
                        Console.WriteLine("Your password expired!");
                    }

                    while (app.m_loginManager.GetForcedPasswordChange())
                    {
                        CommandUserChangePassword();
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Failed to login.");
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Error: User does not exist.");
            }
        }

        public static void CommandAdminRegister()
        {
            Console.Write("Enter login for new account: ");
            string login = Console.ReadLine();
            if (login == null)
            {
                Console.WriteLine("Error: Failed to register account. Login not specified.");
            }

            var app = Application.GetInstance();

            if (!app.m_loginManager.UserExists(ref login))
            {
                Console.Write("Enter password: ");
                string pwd = Console.ReadLine();
                if (app.m_loginManager.UserRegister(ref login, ref pwd))
                {
                    Console.Clear();
                    Console.WriteLine("Successfully registered.");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Error: Failed to register.");
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Error: Failed to register account. User with this name already exists.");
            }
        }

        public static void CommandUserLogout()
        {
            Application.GetInstance().m_loginManager.UserLogout();
        }

        public static void CommandUserChangePassword()
        {
            Console.Write("Enter new password: ");
            string newPWD1 = Console.ReadLine();
            Console.Write("Enter new password again: ");
            string newPWD2 = Console.ReadLine();

            if (newPWD1 != newPWD2)
            {
                Console.WriteLine("Error: Password does not match");
                return;
            }

            Application.GetInstance().m_loginManager.UserChangePassword(ref newPWD1);
            Console.Clear();
            Console.WriteLine("Successfully changed password!");
        }

        public static void CommandUserGetReservations()
        {
            Console.WriteLine("Your reservations: ");
            string name = Application.GetInstance().m_loginManager.GetName();
            var reservations = Application.GetInstance().m_reservationManager.GetUserReservations(ref name);
            Console.WriteLine(" ID | DATE | CAR | USER_ID");
            foreach (var r in reservations)
            {
                Console.WriteLine(r.ToString());
            }
        }

        public static void CommandUserAddReservation()
        {
            Console.Write("Enter car id: ");
            string car_id = Console.ReadLine();
            if (String.IsNullOrEmpty(car_id))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            int carID = -1;
            try
            {
                carID = Convert.ToInt32(car_id);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            
            if(Application.GetInstance().m_reservationManager.CarExists(ref carID)) {
                if (Application.GetInstance().m_reservationManager.CarHasReservation(ref carID))
                {
                    Console.Clear();
                    Console.WriteLine("ERROR: Car already has reservation");
                    return;
                }

                int resID = Application.GetInstance().m_reservationManager.GetNextReservationId();
                int userID = Application.GetInstance().m_loginManager.GetUserID();
                
                var reservation = new Reservation
                {
                    m_date = DateTime.Now,
                    m_id = resID,
                    m_carId = carID,
                    m_userID = userID
                };
                
                Application.GetInstance().m_reservationManager.ReservationCreate(ref reservation);
                Console.Clear();
                Console.WriteLine("Success.");

            }
            else
            {
                Console.Clear();
                Console.WriteLine("ERROR: Car with this id does not exist");
            }

        }

        public static void CommandUserRemoveReservation()
        {
            Console.Write("ID of reservation you want to remove: ");
            string idString = Console.ReadLine();
            if (String.IsNullOrEmpty(idString))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            
            int id = -1;
            try
            {
                id = Convert.ToInt32(idString);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            string userName = Application.GetInstance().m_loginManager.GetName();
            var myReservations = Application.GetInstance().m_reservationManager.GetUserReservations(ref userName);
            foreach (var res in myReservations)
            {
                if (res.m_id == id)
                {
                    Application.GetInstance().m_reservationManager.ReservationRemove(ref res.m_id);
                    Console.Clear();
                    Console.WriteLine("Success!");
                    return;
                }
            }
            Console.Clear();
            Console.WriteLine("ERROR: ID does not exist or belong to your account.");
        }

        public static void CommandAdminRemoveUser()
        {
            Console.Write("Name of user to remove: ");
            string login = Console.ReadLine();

            if (String.IsNullOrEmpty(login))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input");
                return;
            }

            var app = Application.GetInstance();

            if (app.m_loginManager.UserExists(ref login))
            {
                app.m_loginManager.UserRemove(ref login);
                Console.Clear();
                Console.WriteLine("Success.");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Error: User does not exist");
            }

        }

        public static void CommandAdminExpireUserPassword()
        {
            Console.Write("Enter name of user which password will expire: ");
            string login = Console.ReadLine();

            if (String.IsNullOrEmpty(login))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input");
                return;
            }

            var app = Application.GetInstance();

            if (app.m_loginManager.UserExists(ref login))
            {
                app.m_loginManager.UserForcePasswordExpire(ref login);
                Console.Clear();
                Console.WriteLine("Success.");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Error: User does not exist");
            }

        }

        public static void CommandAdminRemoveCar()
        {
            Console.Write("Enter ID of car to remove: ");
            string input = Console.ReadLine();
            try
            {
                int result = Convert.ToInt32(input);

                if (Application.GetInstance().m_reservationManager.CarExists(ref result))
                {
                    Application.GetInstance().m_reservationManager.CarRemove(ref result);
                    Console.Clear();
                    Console.WriteLine("Success.");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("ERROR: Car with specified id does not exist.");
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("ERROR: Invalid input!");
                return;
            }
        }

        public static void CommandAdminAddCar()
        {
            Console.Write("Specify car label:");
            string label = Console.ReadLine();
            if (String.IsNullOrEmpty(label))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            
            Console.Write("Specify car model:");
            string model = Console.ReadLine();
            if (String.IsNullOrEmpty(model))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            
            Console.Write("Specify car type (P = Passenger car, T = Truck):");
            string typeString = Console.ReadLine();
            if (String.IsNullOrEmpty(typeString) || (typeString != "P" && typeString != "T"))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            CarType type = CarType.PASSENER_CAR;
            if (typeString == "P") { type = CarType.PASSENER_CAR; }
            if (typeString == "T") { type = CarType.TRUCK; }
            
            Console.Write("Specify car usage per 100 km:");
            string per100kmString = Console.ReadLine();
            if (String.IsNullOrEmpty(per100kmString))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            decimal per100km;
            try
            {
                per100km = Convert.ToDecimal(per100kmString);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            bool allEntered = false;

            var services = new List<Service>(128);
            
            while (!allEntered)
            {
                Console.WriteLine("Enter services for car: (enter DONE to finish)");
                Console.Write("    Enter service id: ");
                string serviceIDString = Console.ReadLine();
                if (String.IsNullOrEmpty(serviceIDString))
                {
                    Console.Clear();
                    Console.WriteLine("Error: Invalid input!");
                    return;
                }
                if (serviceIDString == "DONE")
                {
                    allEntered = true;
                    break;
                }
                int serviceID = Convert.ToInt32(serviceIDString);
                
                Console.Write("    Enter service price: ");
                string servicePriceString = Console.ReadLine();
                if (String.IsNullOrEmpty(servicePriceString))
                {
                    Console.Clear();
                    Console.WriteLine("Error: Invalid input!");
                    return;
                }
                decimal servicePrice = Convert.ToDecimal(servicePriceString);
                
                
                Console.Write("    Enter service date: ");
                string serviceDateString = Console.ReadLine();
                if (String.IsNullOrEmpty(serviceDateString))
                {
                    Console.Clear();
                    Console.WriteLine("Error: Invalid input!");
                    return;
                }
                DateTime serviceDate = Convert.ToDateTime(serviceDateString);

                services.Add(new Service
                {
                    m_date = serviceDate,
                    m_id = serviceID,
                    m_price = servicePrice
                });
            }

            int id = Application.GetInstance().m_reservationManager.GetNextCarId();
            
            var result = new Car
            {
                m_id = id,
                m_model = model,
                m_per100km = per100km,
                m_services = services,
                m_type = type,
                m_carLabel = label
            };
            
            Application.GetInstance().m_reservationManager.CarSave(ref result);
            Console.Clear();
            Console.WriteLine("Success!");
        }

        public static void CommandAdminListReservationsByUser()
        {
            Console.Write("Specify user login:");
            string login = Console.ReadLine();
            if (String.IsNullOrEmpty(login))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            if (!Application.GetInstance().m_loginManager.UserExists(ref login))
            {
                Console.Clear();
                Console.WriteLine("ERROR: User does not exist");
                return;
            }

            var userRes = Application.GetInstance().m_reservationManager.GetUserReservations(ref login);
            Console.WriteLine(" ID | DATE | CAR | USER_ID");
            foreach (var r in userRes)
            {
                Console.WriteLine(r.ToString());
            }
        }

        public static void CommandAdminListReservationsByCar()
        {
            Console.Write("Specify car id:");
            string carIDString = Console.ReadLine();
            if (String.IsNullOrEmpty(carIDString))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            int carID = -1;
            try
            {
                carID = Convert.ToInt32(carIDString);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            var userRes = Application.GetInstance().m_reservationManager.GetCarReservations(ref carID);
            Console.WriteLine(" ID | DATE | CAR | USER_ID");
            foreach (var r in userRes)
            {
                Console.WriteLine(r.ToString());
            }
        }
        
        public static void CommandAdminListReservations()
        {
            Console.WriteLine(" ID | DATE | CAR | USER_ID");

            var reservations = Application.GetInstance().m_reservationManager.GetAllReservations();

            foreach (var r in reservations)
            {
                Console.WriteLine(r.ToString());
            }

        }

        public static void CommandUserListCars()
        {
            var cars = Application.GetInstance().m_reservationManager.CarGetList();

            Console.WriteLine("All cars:");
            
            foreach (var car in cars)
            {
                Console.WriteLine(" ID | LABEL | MODEL | CAR TYPE | PER_100KM | [SERVICES]");
                Console.WriteLine("SERVICE:  ID - PRICE - DATE §");
                Console.WriteLine(car.ToString());
            }
        }

        public static void CommandAdminRemoveReservation()
        {
            Console.Write("ID of reservation you want to remove: ");
            string idString = Console.ReadLine();
            if (String.IsNullOrEmpty(idString))
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }
            
            int id = -1;
            try
            {
                id = Convert.ToInt32(idString);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: Invalid input!");
                return;
            }

            var reservations = Application.GetInstance().m_reservationManager.GetAllReservations();
            foreach (var res in reservations)
            {
                if (res.m_id == id)
                {
                    Application.GetInstance().m_reservationManager.ReservationRemove(ref res.m_id);
                    Console.Clear();
                    Console.WriteLine("Success!");
                    return;
                }
            }
            Console.Clear();
            Console.WriteLine("ERROR: ID does not exist.");
        }
    } 

    public class Command
    {
        public Command(Delegate callback,string helpMessage)
        {
            m_callback = callback;
            m_helpMessage = helpMessage;
        }

        public Delegate m_callback;
        public string m_helpMessage;
    }
}