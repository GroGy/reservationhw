using System;
using System.Collections.Generic;
using System.Text;

namespace DURezervace.Application
{
    public enum CarType : int
    {
        PASSENER_CAR = 0,
        TRUCK = 1
    }

    public class Car
    {
        public int m_id;
        public string m_carLabel;
        public string m_model;
        public CarType m_type;
        public decimal m_per100km;
        public List<Service> m_services;

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(m_id);
            builder.Append('|');
            builder.Append(m_carLabel);
            builder.Append('|');
            builder.Append(m_model);
            builder.Append('|');
            builder.Append((int)m_type);
            builder.Append('|');
            builder.Append(m_per100km);
            builder.Append('|');

            for(int i = 0; i < m_services.Count;i++)
            {
                builder.Append(m_services[i].m_id);
                builder.Append('-');
                builder.Append(m_services[i].m_price);
                builder.Append('-');
                builder.Append(m_services[i].m_date);

                if (i != m_services.Count - 1)
                {
                    builder.Append('§');
                }
            }

            return builder.ToString();
        }

        public static Car CreateFromSerializedString(ref string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new Exception("Failed to parse reservation file");
            }

            string[] values = data.Split('|');

            if (values.Length < 6)
            {
                throw new Exception("Failed to parse reservation file");
            }
            
            // ID | LABEL | MODEL | CAR TYPE | PER_100KM | [SERVICES]
            
            // SERVICE:  ID - PRICE - DATE §
            
            List<Service> services = new List<Service>();
            
            var valuesReservation = values[5].Split('§');
            
            foreach(var a in valuesReservation)
            {
                var serviceData = a;
                try
                {
                    services.Add(Service.CreateFromSerializedString(ref serviceData));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            var result = new Car
            {
                m_id = Convert.ToInt32(values[0]),
                m_carLabel = values[1],
                m_model = values[2],
                m_type = (CarType)(Convert.ToInt32(values[3])),
                m_per100km = Convert.ToDecimal(values[4]),
                m_services = services
            };

            return result;
        }
    }
}